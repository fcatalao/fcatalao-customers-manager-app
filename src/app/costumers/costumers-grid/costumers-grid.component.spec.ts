import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostumersGridComponent } from './costumers-grid.component';

describe('CostumersGridComponent', () => {
  let component: CostumersGridComponent;
  let fixture: ComponentFixture<CostumersGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostumersGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostumersGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
