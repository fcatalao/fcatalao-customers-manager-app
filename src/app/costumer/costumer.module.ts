import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CostumerRoutingModule } from './costumer-routing.module';
import { CostumerDetialsComponent } from './costumer-detials/costumer-detials.component';
import { CostumerDetailsComponent } from './costumer-details/costumer-details.component';
import { CostumerComponent } from './costumer.component';

@NgModule({
  declarations: [CostumerDetialsComponent, CostumerDetailsComponent, CostumerComponent],
  imports: [
    CommonModule,
    CostumerRoutingModule
  ]
})
export class CostumerModule { }
