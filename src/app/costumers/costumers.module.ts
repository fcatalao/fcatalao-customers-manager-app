import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CostumersRoutingModule } from './costumers-routing.module';
import { CostumersCardComponent } from './costumers-card/costumers-card.component';
import { CostumersGridComponent } from './costumers-grid/costumers-grid.component';
import { CostumersComponent } from './costumers.component';

@NgModule({
  declarations: [CostumersCardComponent, CostumersGridComponent, CostumersComponent],
  imports: [
    CommonModule,
    CostumersRoutingModule
  ]
})
export class CostumersModule { }
